<?php

/**
 * ClassBeeQueen
 *
 * @abstract
 * @package
 * @version 1.3
 * @date Sat Jul  4 20:44:16 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
abstract class ClassBeeQueen
{
	/**
	 * url - the URL to drone
	 *
	 * @var string
	 * @access private
	 */
	private $url = '';

	/**
	 * key - the key of an array
	 *
	 * @var string
	 * @access private
	 */
	private static $key = 0;

	/**
	 * value
	 *
	 * @var string
	 * @access private
	 */
	private static $value = 0;

	/**
	 * filename - file name to store the data
	 *
	 * @var string
	 * @access private
	 */
	private static $filename = '';

	/**
	 * array - empty array
	 *
	 * @var string
	 * @access private
	 */
	private static $array = array();

	/**
	 * stock - the stock to check
	 *
	 * @var string
	 * @access private
	 */
	private static $stock = '';


	/**
	 * stocks - array of stocks to check
	 *
	 * @var array
	 * @access private
	 */
	private static $stocks = array();

	/**
	 * __toString
	 *
	 * @access public
	 * @return void
	 */
	public function __toString()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return $this->url;
	}

	/**
	 * getData - fetches the data received from the bee drone
	 * and returns an unique array
	 *
	 * @param mixed $newKey
	 * @param mixed $newValue
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getData($newKey, $newValue)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$key = $newKey;
		self::$value = $newValue;

		$result = array();
		foreach ($newKey as $i => $k)
		{
			$result[$k][] = $newValue[$i];
		}
		return self::uniqueArray($result);
	}

	/**
	 * compareArray - diff or merge 2 arrays
	 *
	 * @param array $newArray1 - the array received from the bee drone
	 * @param array $newArray2 - the array retrieved from the file
	 * @param mixed $type - diff or merge
	 * @static
	 * @access public
	 * @return void
	 */
	public static function compareArray(array $newArray1, array $newArray2, $type)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$result = array();

		foreach ($newArray1 as $key => $val)
		{
			if(is_array($newArray2) && $type != null)
			{
				if($type == "diff")
				{
					// echo "We are in diff".PHP_EOL;
					$result = array_values(array_diff($newArray1[$key], $newArray2[$key]));
				}
				elseif ($type == "merge")
				{
					// echo "We are in merge".PHP_EOL;
					$result = array_values(array_merge($newArray1[$key], $newArray2[$key]));
				}
				else return null;
			}
		}
		return array($key => $result);
	}

	/**
	 * uniqueArray - creates an unique array for a 2 dimenssional array
	 *
	 * @param array $newArray
	 * @static
	 * @access private
	 * @return void
	 */
	private static function uniqueArray(array $newArray)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		foreach ($newArray as $key => $value)
		{
			if (is_array($value))
			{
				if (count($value) == count(array_unique($value)))
					$result[$key] = $value;
				else
					$result[$key] = array_values(array_unique($value));
			}
			else
				$result[$key] = $value;
		}
		return $result;
	}

	/**
	 * storeData - stores a serialized array to a file
	 *
	 * @param array $newArray - the array to store
	 * @param mixed $newFilename - file name where the serialized array will be stored
	 * @static
	 * @access public
	 * @return void
	 */
	public static function storeData(array $newArray, $newFilename)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$filename = $newFilename;
		file_put_contents($newFilename, serialize($newArray), LOCK_EX);
	}

	/**
	 * retreiveData - retrieves the data from a file
	 *
	 * @param mixed $newFilename
	 * @static
	 * @access public
	 * @return void - the unserialized array
	 */
	public static function retreiveData($newFilename)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$filename = $newFilename;
		return unserialize(file_get_contents($newFilename));
	}

	/**
	 * retrieveKey - retrieves the key of an array
	 *
	 * @param array $newArray
	 * @static
	 * @access public
	 * @return void - the first key of the array
	 */
	public static function retrieveKey(array $newArray)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$array = $newArray;
		$keys = array_keys($newArray);
		return array_shift($keys);
	}

	/**
	 * setStock - sets the stock number to check
	 *
	 * @param string $newStock
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setStock($newStock)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$stock = $newStock;
		return $newStock;
	}

	/**
	 * getStock  - gets the stock number to check
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getStock()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$stock;
	}

	/**
	 * getStockArgs - experimental/ not used yet
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getStockArgs()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$args = func_get_args();

		if(isset ($args[0]))
			return $args[0];
		else return '6666';
	}

	/**
	 * searchArrayRecursive - recursive look inside the array for values
	 *
	 * @static
	 * @access private
	 * @return void - all the keys
	 */
	private static function searchArrayRecursive()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$args = func_get_args();
		if(!(isset($args[0]) or isset($args[1])))
		{
			Logger::getLogger('file')->fatal("\t".__FUNCTION__." takes 2 arguments.".BN);
			exit(__FUNCTION__." takes 2 arguments.");
		}
		elseif (!is_array($args[1]))
		{
			Logger::getLogger('file')->fatal("\tArgument 2 in ".__FUNCTION__." must be an array.".BN);
			exit("Argument 2 in ".__FUNCTION__." must be an array.");
		}
		else
		{
			$results = array();
			foreach ($args[1] as $key => $arr)
			{
				if(is_array($arr))
				{
					$ret = self::searchArrayRecursive($args[0], $arr);

					if ($ret != false)
						$results[] = $ret;
				}
				else
					if($arr == null)
						$results[] = $key;
			}
			return $results;
		}
	}

	/**
	 * printNullArray - print all the null keys
	 *
	 * @static
	 * @access public
	 * @return void - string of all the null keys
	 */
	public static function printNullArray()
	{
		$args = func_get_args();
		if(!isset($args[0]) or !is_array($args[0]))
		{
			Logger::getLogger('cli')->fatal("\t".__FUNCTION__." takes 1 argument and it must be an array.".BN);
			exit(__FUNCTION__." takes 1 argument and it must be an array.");
		}
		else
		{
			$arr = self::searchArrayRecursive(null,$args[0]);
			$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($arr));
			$null = iterator_to_array($iterator, false);
			return implode(',', $null);
		}
	}

	public function __destruct(){}
}
