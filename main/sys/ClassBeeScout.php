<?php

/**
 * ClassBeeScout
 *
 * @uses ClassBee
 * @package
 * @version 1.1
 * @date Sat Jul  4 20:44:40 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeScout extends ClassBee
{
	private static $beeType = "Scout";

	/**
	 * ClassBeeScout
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeScout()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::setBeeType(self::$beeType);
	}

	function __destruct(){}
}
