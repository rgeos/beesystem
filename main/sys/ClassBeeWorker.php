<?php

/**
 * ClassBeeWorker
 *
 * @uses ClassBee
 * @package
 * @version 1.2
 * @date Sat Jul  4 20:45:03 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeWorker extends ClassBee
{
	private static $beeType = "Worker";
	private static $page = "";

	/**
	 * ClassBeeWorker
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeWorker()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::setBeeType(self::$beeType);
	}

	public static function getPage($newPage)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$page = $newPage;

		// using an external class
		// dom/simple_html_dom.php
		new simple_html_dom();
		return file_get_html($newPage);
	}

	function __destruct(){}
}
