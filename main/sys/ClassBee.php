<?php

/**
 * ClassBee - super class
 *
 * @package
 * @version 0.2
 * @date Sat Jul  4 20:39:53 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBee
{
	private static $beeType;
	private static $url;

	/**
	 * setBeeType - sets the type of bee.
	 * There can be several types of bees:
	 *	> drone - fetches pages headers
	 *	> worker - fetches the whole DOM
	 *	> scout - searches the web for relevant content
	 *	> curator - cleans up DOM contents
	 *	> maker - builds a view from contents
	 *
	 * @param mixed $newBeeType
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setBeeType($newBeeType)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$beeType = $newBeeType;
	}


	/**
	 * getBeeType - returns the type of bee
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getBeeType()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$beeType;
	}


	/*
	 * setUrl - sets the URL from where the heades will be fetched
	 *
	 * @param mixed $newUrl
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setUrl($newUrl)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$url = $newUrl;
	}


	/**
	 * getUrl - returns the URL
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getUrl()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$url;
	}

	public function __destruct() {}

}
