<?php

/**
 * ClassBeeDrone
 *
 * @uses ClassBee
 * @package
 * @version 0.2
 * @date Sat Jul  4 20:41:10 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeDrone extends ClassBee
{
	private static $beeType = "Drone";
	private static $url;
	private static $string;

	/**
	 * ClassBeeDrone - this bee is fetching DOM headers
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeDrone()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::setBeeType(self::$beeType);
	}

	// TODO - second argument should be an array
	// TODO - check if the $newUrl exists
	/**
	 * getHeaders - returns all the headers unless $type is set
	 *
	 * @param mixed $newUrl - the URL
	 * @param mixed $type - the header element
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getHeaders($newUrl, $type = null)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$url = $newUrl;
		$headers = get_headers($newUrl,1);
		if($newUrl != null)
		{
			if ($type != null)
				return $headers[$type];
			elseif($type == null)
				return $headers;
		} else return null;
	}

	/**
	 * hashString - hasing a string
	 *
	 * @param mixed $newString
	 * @static
	 * @access public
	 * @return void
	 */
	public static function hashString($newString)
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$string = $newString;
		return hash('sha512', $newString);
	}

	function __destruct(){}
}
