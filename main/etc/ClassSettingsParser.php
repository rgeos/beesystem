<?php


/**
 * ClassSettingsParser
 *
 * @version 0.2
 * @date Sun Aug  2 15:20:57 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com>
 * @license GPL3
 */

class ClassSettingsParser
{
	private static $filename = '';

	function ClassSettingsParser(){}

	public static function setIniFile($newIniFile) { self::$filename = $newIniFile; }

	public static function getIniFile() { return self::$filename; }

	public static function readIniFile($bySection = FALSE)
	{
		return parse_ini_file(self::getIniFile(), $bySection);
	}

	public static function randomServer($array = array())
	{
		if (is_array($array)) {
			shuffle($array);
			return $array[0];
		}
		else return null;
	}

	public static function shuffleArray($array = array())
	{
		if(is_array($array))
		{
			shuffle($array);
			return implode(",", $array);
		}
		else return null;
	}

	public static function serverSettings($section, $subSection = NULL)
	{
		if(isset($subSection) && $subSection !== NULL)
			return self::readIniFile(true)[$section][$subSection];
		else
			return self::readIniFile(true)[$section];

	}

	function __destruct(){}
}