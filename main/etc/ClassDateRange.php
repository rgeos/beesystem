<?php
/**
 * This class creates a date range while skipping Sat and Sun
 *
 * @package
 * @version 0.1
 * @date Sat Jul  4 10:25:41 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassDateRange
{

	private static $start	= '1983-01-03';
	private static $end		= '2082-12-30';
	private static $step	= '+1 day';
	private static $format	= 'Y-n-j';

	/**
	 * constructor
	 */
	function ClassDateRange(){}

	/**
	 * Starting date of the range
	 * @param string $newStartDate
	 */
	public static function setStartDate($newStartDate) { self::$start = $newStartDate; }

	/**
	 * Return the setted start date
	 * @return string
	 */
	public static function getStartDate(){ return self::$start; }

	/**
	 * Ending date of the range
	 * @param string $newEndDate
	 */
	public static function setEndDate($newEndDate){ self::$end = $newEndDate; }

	/**
	 * Return the setted end date
	 * @return string
	 */
	public static function getEndDate(){ return self::$end; }

	/**
	 * The interval between 2 consecutive dates
	 * @param string $newStepDate
	 */
	public static function setStepDate($newStepDate){ self::$step = $newStepDate; }

	/**
	 * Return the interval between 2 consecutive dates
	 * @return string
	 */
	public static function getStepDate(){ return self::$step; }

	/**
	 * Set the format of the date
	 * Default case is Y-n-j
	 * Y = year
	 * n = numeric representation of the month without the leading zeros
	 * j = day of the month without the leading zeros
	 * @param string $newFormatDate
	 */
	public static function setFormatDate($newFormatDate){ self::$format = $newFormatDate; }

	/**
	 * Return the format of the date
	 * @return string
	 */
	public static function getFormatDate(){ return self::$format; }

	/**
	 * Create the date range skipping Saturdays and Sundays
	 * @return multitype:string
	 */
	public static function dateRange()
	{
		$current = strtotime(self::getStartDate());
		$last = strtotime(self::getEndDate());

		$dates = array();

		if(isset(self::$step))
			$step = self::getStepDate();
		else
			$step = self::$step;

		if(isset(self::$format))
			$format = self::getFormatDate();
		else
			$format = self::$format;

		while($current < $last)
		{
			$dayofweek = date('N', $current);
			if(!($dayofweek == 6 || $dayofweek ==7)) // No Sat or Sun
				$dates[] = date($format, $current);
			$current = strtotime($step,$current);
		}

		return $dates;
	}

}