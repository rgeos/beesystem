<?php

/**
 * ClassTranslations
 *
 * @version 1.5
 * @date Thu Jul 16 21:08:57 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com>
 * @license GPL3
 */
class ClassTranslations
{
	private static $elem = '';
	private static $matches = '';

	/**
	 * ClassTranslations - translates info fields from Jp to En
	 *
	 */
	function ClassTranslations() {
		//Logger::getLogger(get_called_class())->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
	}

	/**
	 * Translate several keywords from Jp to En
	 * @return void|string|number|mixed
	 */
	public static function translate()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$arg = func_get_arg(0);
		switch ($arg)
		{
			// quants - KabuMap
		case '成長性':
			return self::$elem = 'growth';
			break;

		case '割安性':
			return self::$elem = 'valuation';
			break;

		case '企業規模':
			return self::$elem = 'scale';
			break;

		case 'テクニカル':
			return self::$elem = 'technical';
			break;

		case '財務健全性':
			return self::$elem = 'solvency';
			break;

		case '市場ﾄﾚﾝﾄﾞ':
			return self::$elem = 'trend';
			break;

			//research - MinKabu
		case '割安':
			return self::$elem = -1; //'undervalued';
			break;

		case "割高":
			return self::$elem = 1; //'overvalued';
			break;

		case "妥当圏内":
			return self::$elem = 0; //'even_valued';
			break;

			// votes - MinKabu
		case '売り傾向':
			return self::$elem = -3; //'sell_0_3';
			break;

		case '売り一ツ星':
			return self::$elem = -4; //'sell_1_3';
			break;

		case '売り二ツ星':
			return self::$elem = -5; //'sell_2_3';
			break;

		case '売り三ツ星':
			return self::$elem = -6; //'sell_3_3';
			break;

		case '買い傾向':
			return self::$elem = 3; //'buy_0_3';
			break;

		case '買い一ツ星':
			return self::$elem = 4; //'buy_1_3';
			break;

		case '買い二ツ星':
			return self::$elem = 5; //'buy_2_3';
			break;

		case '買い三ツ星':
			return self::$elem = 6; //'buy_3_3';
			break;

			// index
		case '出来高':
			return self::$elem = 'volume';
			break;

		case '始値':
			return self::$elem = 'open';
			break;

		case '高値':
			return self::$elem = 'high';
			break;

		case '安値':
			return self::$elem = 'low';
			break;

		case '時価総額':
			return self::$elem = 'market_value';
			break;

		case '単元株数':
			return self::$elem = 'units';
			break;

		case '発行済株式数':
			return self::$elem = 'issued_shares';
			break;

		case '配当利回り':
			return self::$elem = 'dividend_yield';
			break;

		case '配当性向(単体)':
			return self::$elem = 'payout_ratio';
			break;

		case 'PER（調整後）':
			return self::$elem = 'post_adjustment_PER';
			break;

		case '株':
		case '円':
		case '倍':
		case '%':
			return;
			break;

		case '百万円':
			return self::$elem = '000000';
			break;

		case '千株':
			return self::$elem = '000';
			break;

			// market names
		case 'JASDAQグロース':
			return self::$elem = 'jasdaq_growth';
			break;

		case 'JASDAQスタンダード':
			return self::$elem = 'jasdaq_standard';
			break;

		case 'アンビシャス':
			return self::$elem = 'ambitious';
			break;

		case 'セントレックス':
			return self::$elem = 'centrex';
			break;

		case '名証一部':
			return self::$elem = 'nse1';
			break;

		case '名証二部':
			return self::$elem = 'nse2';
			break;

		case '札証':
			return self::$elem = 'sse';
			break;

		case '東証マザーズ':
			return self::$elem = 'mothers';
			break;

		case '東証一部':
			return self::$elem = 'tse1';
			break;

		case '東証二部':
			return self::$elem = 'tse2';
			break;

		case '福証':
			return self::$elem = 'fse';
			break;

			// counties
		case '三重県':
			return self::$elem = 'mie';
			break;

		case '京都府':
			return self::$elem = 'kyoto';
			break;

		case '佐賀県':
			return self::$elem = 'saga';
			break;

		case '兵庫県':
			return self::$elem = 'hyogo';
			break;

		case '北海道':
			return self::$elem = 'hokkaido';
			break;

		case '千葉県':
			return self::$elem = 'chiba';
			break;

		case '和歌山県':
			return self::$elem = 'wakayama';
			break;

		case '埼玉県':
			return self::$elem = 'saitama';
			break;

		case '大分県':
			return self::$elem = 'oita';
			break;

		case '大阪府':
			return self::$elem = 'osaka';
			break;

		case '奈良県':
			return self::$elem = 'nara';
			break;

		case '宮城県':
			return self::$elem = 'miyagi';
			break;

		case '宮崎県':
			return self::$elem = 'miyazaki';
			break;

		case '富山県':
			return self::$elem = 'fukuyama';
			break;

		case '山口県':
			return self::$elem = 'yamaguchi';
			break;

		case '山形県':
			return self::$elem = 'yamagata';
			break;

		case '山梨県':
			return self::$elem = 'yamanashi';
			break;

		case '岐阜県':
			return self::$elem = 'gifu';
			break;

		case '岡山県':
			return self::$elem = 'okayama';
			break;

		case '岩手県':
			return self::$elem = 'iwate';
			break;

		case '島根県':
			return self::$elem = 'shimane';
			break;

		case '広島県':
			return self::$elem = 'hiroshima';
			break;

		case '徳島県':
			return self::$elem = 'tokushima';
			break;

		case '愛媛県':
			return self::$elem = 'ehime';
			break;

		case '愛知県':
			return self::$elem = 'aichi';
			break;

		case '新潟県':
			return self::$elem = 'niigata';
			break;

		case '東京都':
			return self::$elem = 'tokyo';
			break;

		case '栃木県':
			return self::$elem = 'tochigi';
			break;

		case '沖縄県':
			return self::$elem = 'okinawa';
			break;

		case '滋賀県':
			return self::$elem = 'shiga';
			break;

		case '熊本県':
			return self::$elem = 'kumamoto';
			break;

		case '石川県':
			return self::$elem = 'ishikawa';
			break;

		case '神奈川県':
			return self::$elem = 'kanagawa';
			break;

		case '福井県':
			return self::$elem = 'fukui';
			break;

		case '福岡県':
			return self::$elem = 'fukuoka';
			break;

		case '福島県':
			return self::$elem = 'fukushima';
			break;

		case '秋田県':
			return self::$elem = 'akita';
			break;

		case '米国':
			return self::$elem = 'US';
			break;

		case '群馬県':
			return self::$elem = 'gunma';
			break;

		case '茨城県':
			return self::$elem = 'ibaraki';
			break;

		case '長崎県':
			return self::$elem = 'nagasaki';
			break;

		case '長野県':
			return self::$elem = 'nagano';
			break;

		case '青森県':
			return self::$elem = 'aomori';
			break;

		case '静岡県':
			return self::$elem = 'shizuoka';
			break;

		case '香川県':
			return self::$elem = 'kagawa';
			break;

		case '高知県':
			return self::$elem = 'kouchi';
			break;

		case '鳥取県':
			return self::$elem = 'tottori';
			break;

		case '鹿児島県':
			return self::$elem = 'kagoshima';
			break;

			// industries

		case 'その他製造':
			return self::$elem = 'other_manufacturing';
			break;

		case 'その他金融':
			return self::$elem = 'other_financial';
			break;

		case 'ゴム':
			return self::$elem = 'rubber';
			break;

		case 'サービス':
			return self::$elem = 'services';
			break;

		case 'パルプ・紙':
			return self::$elem = 'pulp_paper';
			break;

		case '不動産':
			return self::$elem = 'real_estate';
			break;

		case '保険':
			return self::$elem = 'insurance';
			break;

		case '倉庫・運輸関連':
			return self::$elem = 'wearhouse_transportation';
			break;

		case '化学':
			return self::$elem = 'chemicals';
			break;

		case '医薬品':
			return self::$elem = 'pharmaceuticals';
			break;

		case '卸売':
			return self::$elem = 'wholesale';
			break;

		case '小売':
			return self::$elem = 'retail';
			break;

		case '建設':
			return self::$elem = 'construction';
			break;

		case '情報・通信':
			return self::$elem = 'media_telecoms';
			break;

		case '機械':
			return self::$elem = 'machinery';
			break;

		case '水産・農林':
			return self::$elem = 'fishery_agriculture_forestry';
			break;

		case '石油・石炭製品':
			return self::$elem = 'oil_coal';
			break;

		case '窯業':
			return self::$elem = 'glass_ceramics';
			break;

		case '精密機器':
			return self::$elem = 'precision_instruments';
			break;

		case '繊維':
			return self::$elem = 'textiles';
			break;

		case '証券、商品先物取引':
			return self::$elem = 'securities_futures';
			break;

		case '輸送用機器':
			return self::$elem = 'transportation_equipment';
			break;

		case '金属製品':
			return self::$elem = 'metal';
			break;

		case '鉄鋼':
			return self::$elem = 'iron_steel';
			break;

		case '鉱業':
			return self::$elem = 'mining';
			break;

		case '銀行':
			return self::$elem = 'banking';
			break;

		case '陸運・海運・空運':
			return self::$elem = 'air_land_sea_transportation';
			break;

		case '電力・ガス':
			return self::$elem = 'power_gas';
			break;

		case '電気機器':
			return self::$elem = 'electric_appliances';
			break;

		case '非鉄金属':
			return self::$elem = 'nonfferouse_metals';
			break;

		case '食品':
			return self::$elem = 'foods';
			break;

		case '官公庁':
			return self::$elem = 'public_office';
			break;

		case '非上場・外資系企業':
			return self::$elem = 'foreign_direct_investment';
			break;

		default:
			return $arg;
			break;
		}
	}

	/**
	 * Translate multiple arguments
	 * @return multitype:Ambigous <void, string, number, mixed>
	 */
	public static function multiTranslate()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$args = func_get_args();

		if(empty($args[0]) or !isset($args[1]))
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 2 arguments - ".BN);
			exit("Function ".__FUNCTION__." takes 2 arguments");
		}
		else
		{
			$array = explode($args[0], $args[1]);

			$translated = array();
			foreach($array as $arr)
				$translated[] = self::translate($arr);

			//$string = implode($args[0], $translated);
			return $translated;
		}
	}

	/**
	 * remove commas from the number
	 * @return number
	 */
	public static function remove_commas()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$arg = func_get_arg(0);

		$no_comma = preg_replace('/,/', '', $arg);
		$pattern = '/(^[-+]?[0-9]*\.?[0-9]+)(.*)/';

		preg_match($pattern, $no_comma, self::$matches);

		if(isset(self::$matches[1]) or isset(self::$matches[2]))
			return floatval(preg_replace($pattern, self::$matches[1].self::translate(self::$matches[2]), $no_comma));
	}

	/**
	 * Cleanup the date
	 * @return multitype:ISO format date
	 */
	public static function clean_date()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$arg = func_get_arg(0);

		$pattern = '/([0-9]{4})(年)([0-9]{1,2})(月)([0-9]{1,2})(日)/';

		preg_match($pattern, $arg, self::$matches);
		/**
		 * create something like
		 * [date] => Array
         *       (
         *           [y] => 2014
         *           [m] => 9
         *           [d] => 9
         *       )
		 */

		$string = explode('-',preg_replace($pattern, self::$matches[1].'-'.self::$matches[3].'-'.self::$matches[5], $arg));

		$date = new DateTime(intval($string[0])."-".intval($string[1])."-".intval($string[2])." 00:00:00", new DateTimeZone("Asia/Tokyo"));
		return ['date' => $date->format('c')];
	}

	/**
	 * Division of 2 numbers
	 * @return number
	 */
	public static function calculate()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$arg = func_get_arg(0);
		$args = explode('/', $arg);
		return intval($args[0])/intval($args[1]);
	}

	function __destruct(){}
}
