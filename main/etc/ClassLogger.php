<?php

/**
 * ClassLogger
 *
 * @uses LoggerConfigurator
 * @version 1.3
 * @date Tue Aug 25 08:14:04 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com>
 * @license GPL3
 */
class ClassLogger implements LoggerConfigurator {

	/**
	 * configure
	 *
	 * @param LoggerHierarchy $hierarchy
	 * @param mixed $input
	 */
	public function configure(LoggerHierarchy $hierarchy, $input = null) {

		// layout for File appenders
		$layoutFile = new LoggerLayoutPattern();
		$layoutFile->setConversionPattern("%d{Y-m-d H:i:s} %-5p %c %X{username}: %m in %F at line %L%n");
		$layoutFile->activateOptions();

		$layoutCli = new LoggerLayoutPattern();
		$layoutCli->setConversionPattern("[Logger]: '%logger'\t[Message]: '%msg'\t@line %L%newline");
		$layoutCli->activateOptions();

		// Create an appender which logs to file
		$appFile = new LoggerAppenderFile('file');
		$appFile->setLayout($layoutFile);
		$appFile->setFile("logs".DS."99_script_".gethostname().".log");
		$appFile->setAppend(true);
		$appFile->setThreshold('debug');
		$appFile->activateOptions();

		// Create an appender which echoes log events, using a custom layout
		$appEcho = new LoggerAppenderEcho('cli');
		$appEcho->setLayout($layoutCli);
		$appEcho->setThreshold('warn');
		$appEcho->activateOptions();

		// Add appenders to the root logger
		$root = $hierarchy->getRootLogger();
		$root->addAppender($appFile);
		$root->addAppender($appEcho);
	}

	function __destruct(){}
}
