<?php

/**
 * MongoDB Mapper
 *
 * @package
 * @version 0.2
 * @date Wed Jul  1 00:33:53 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

class ClassMongoMapper
{

	function ClassMongoMapper(){}

	public static function arraySliceByIndex($string, $array)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator(array_keys($array)));
		while($it->valid())
		{
			if( $it->current() === $string )
				return array_slice($array, $it->key(),1,true);
			$it->next();
		}
		return false;
	}

}