<?php

/**
 * MongoDB Wrapper
 *
 * @package
 * @version 0.6
 * @date Sat Aug  1 15:22:31 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

class ClassMongoWrapper
{
	private static $host = '';
	private static $username = '';
	private static $password = '';
	private static $db_name = '';
	private static $authSource = '';
	private static $collection = '';

	function ClassMongoWrapper() {}

	public static function setHost()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(isset($arg))
			self::$host = $arg;
		else
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 1 argument - ".BN);
			exit("\tFunction ".__FUNCTION__." takes 1 argument - ");
		}
	}

	public static function getHost()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$host;
	}

	public static function setUsername()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(isset($arg))
			self::$username = $arg;
		else
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 1 argument - ".BN);
			exit("\tFunction ".__FUNCTION__." takes 1 argument - ");
		}
	}

	public static function getUsername()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$username;
	}

	public static function setPassword()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(isset($arg))
			self::$password = $arg;
		else
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 1 argument - ".BN);
			exit("\tFunction ".__FUNCTION__." takes 1 argument - ");
		}
	}

	private static function getPassword()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$password;
	}

	public static function setDbName()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(isset($arg))
			self::$db_name = $arg;
		else
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 1 argument - ".BN);
			exit("\tFunction ".__FUNCTION__." takes 1 argument - ");
		}
	}

	public static function getDbName()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$db_name;
	}

	public static function setAuthSource()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(isset($arg))
			self::$authSource = $arg;
		else
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 1 argument - ".BN);
			exit("\tFunction ".__FUNCTION__." takes 1 argument - ");
		}
	}

	public static function getAuthSource()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$authSource;
	}

	public static function setCollection()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(isset($arg))
			self::$collection = $arg;
		else
		{
			Logger::getLogger('cli')->fatal("\tFunction ".__FUNCTION__." takes 1 argument - ".BN);
			exit("\tFunction ".__FUNCTION__." takes 1 argument - ");
		}
	}

	public static function getCollection()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$collection;
	}

	public static function createClient()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		return new MongoClient(self::getHost(), [
				'username' => self::getUsername(),
				'password' => self::getPassword(),
				'db' => self::getDbName(),
				'authSource' => self::getAuthSource()]);
	}

	public static function debugConnection()
	{
		$info_connection	= self::createClient()->getConnections();
		$info_dbs			= self::createClient()->listDBs();
		$info_host			= self::createClient()->getHosts();
		$info				= [
				"CONNECTION"	=> $info_connection,
				"DATABASE"		=> $info_dbs,
				"HOST"			=> $info_host];
		return $info;
	}

	public static function selectColl()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return new MongoCollection(self::createClient()->selectDB(self::getDbName()), self::getCollection());
	}

	public static function insertDb($data, $type, $options = array())
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		switch ($type)
		{
			case 'batch':
				self::selectColl()->batchInsert($data, $options);
				break;
			case 'regular':
				self::selectColl()->insert($data, $options);
				break;
			default:
				exit('No data type provided');
		}
	}

	public static function searchDb($query, $options = array(), $limit = 0)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$cursor = self::selectColl()->find($query, $options)->limit(intval($limit));
		return iterator_to_array($cursor);
	}

	public static function updateDb($criteria, $data, $options = ['upsert' => true])
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::selectColl()->update($criteria, $data, $options);
	}

}