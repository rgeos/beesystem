<?php

/**
 * ClassBeeQueenYahooJp
 *
 * @uses ClassBeeQueen
 * @package
 * @version 1.1
 * @date Sat Jul  4 20:29:00 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeQueenYahooJp extends ClassBeeQueen
{
	/**
	 * $url - YahooJp url
	 *
	 * @var string
	 * @access private
	 */
	private $url = 'http://info.finance.yahoo.co.jp/history/';

	/**
	 * $dataType - simple quote data or margin data
	 *
	 * @var string
	 * @access private
	 */
	private static $dataType = '';

	/**
	 * $stock - stock identifier
	 *
	 * @var string
	 * @access private
	 */
	private static $stock = '';

	/**
	 * $dateType - either start/end date
	 *
	 * @var string
	 * @access private
	 */
	private static $dateType = null;

	/**
	 * $startDate
	 *
	 * @var string
	 * @access private
	 */
	private static $startDate = '';

	/**
	 * $endDate
	 *
	 * @var string
	 * @access private
	 */
	private static $endDate = '';

	/**
	 * $intervalType - day/week/month type
	 *
	 * @var string
	 * @access private
	 */
	private static $intervalType = 'day';

	/**
	 * $page - page identifier. Default '1'
	 *
	 * @var string
	 * @access private
	 */
	private static $page = '';

	/**
	 * name - the name of the queen
	 *
	 * @var string
	 * @access private
	 */
	private static $name = 'Queen_YahooJp_';

	/**
	 * ClassBeeQueenYahooJp
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeQueenYahooJp()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$this->__toString();
	}

	/**
	 * __toString
	 *
	 * @access public
	 * @return void
	 */
	public function __toString()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		parent::__toString();
		return $this->url;
	}

	/**
	 * queenName - YahooJp queen
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function queenName()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$name;
	}

	/**
	 * setDataType - set either simple quote data or margin data
	 *
	 * @param mixed $newType
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setDataType($newType)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		if(!isset($newType))
		{
			Logger::getLogger('cli')->fatal("\t".__FUNCTION__." takes 1 argument - ".BN);
			exit(__FUNCTION__." takes 1 argument");
		}

		switch ($newType)
		{
			case 'margin':
				$newType = 'margin/';
				break;
			default:
				$newType = null;
				break;
		}

		self::$dataType = $newType;
	}

	/**
	 * getDataType
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getDataType()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$dataType;
	}

	/**
	 * setStock - set the stock identifier
	 *
	 * @param mixed $newStock
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setStock($newStock)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		if(!isset($newStock))
		{
			Logger::getLogger('cli')->fatal("\t".__FUNCTION__." takes 1 argument - ".BN);
			exit(__FUNCTION__." takes 1 argument");
		}

		$newStock = '?code='.$newStock.'&';
		self::$stock = $newStock;
	}

	/**
	 * getStock
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getStock()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$stock;
	}

	/**
	 * setDate - set the date and type of date (start/end)
	 *
	 * @param mixed $newDate
	 * @param mixed $newDateType
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setDate($newDate, $newDateType)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		self::$dateType = $newDateType;

		if(!preg_match("/\d{8}/", $newDate) || $newDateType == null)
		{
			$newDate = null;
			$newDateType = null;
		}

		$y = $newDate[0].$newDate[1].$newDate[2].$newDate[3];
		$m = $newDate[4].$newDate[5];
		$d = $newDate[6].$newDate[7];

		if ($d < 1 || $d > 31)
			return false;
		if ($m < 1 || $m > 12)
			return false;
		if ($y < 1983 || $y > date('Ymd'))
			return false;

		switch ($newDateType)
		{
			case 'start':
				$newDate = "sy=$y&sm=$m&sd=$d&";
				self::$startDate = $newDate;
				break;
			case 'end':
				$newDate = "ey=$y&em=$m&ed=$d&";
				self::$endDate = $newDate;
				break;
			default:
				$newDate = null;
				break;
		}
	}

	/**
	 * getDate
	 *
	 * @param mixed $dateType
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getDate($dateType)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		if ($dateType != null) {
			switch ($dateType) {
				case 'start':
                	return self::$startDate;
					break;
				case 'end':
					return self::$endDate;
					break;
				default:
					return null;
					break;
			};
		}
	}

	/**
	 * setIntervalType - set interval type (day/week/month)
	 *
	 * @param mixed $newInervalType
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setIntervalType($newInervalType)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		switch ($newInervalType)
		{
			case 'month':
				$newInervalType = "tm=m&";
				break;
			case 'week':
				$newInervalType = "tm=w&";
				break;
			default:
				$newInervalType = "tm=d&";
				break;
		}

		self::$intervalType = $newInervalType;
	}

	/**
	 * getIntervalType
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getIntervalType()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$intervalType;
	}

	/**
	 * setPage - set page id (default 1)
	 *
	 * @param mixed $newPage
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setPage($newPage)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$newPage = 'p='.$newPage;
		self::$page = $newPage;
	}

	/**
	 * getPage
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getPage()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$page;
	}

	function __destruct(){}
}
