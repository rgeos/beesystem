<?php

/**
 * ClassBeeQueenMinKabu
 *
 * @uses ClassBeeQueen
 * @package
 * @version 1.1
 * @date Sat Jul  4 20:26:25 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeQueenMinKabu extends ClassBeeQueen
{
	/**
	 * url - minkabu url
	 *
	 * @var string
	 * @access private
	 */
	private $url = 'http://minkabu.jp/stock/';

	/**
	 * name - the name of the queen
	 *
	 * @var string
	 * @access private
	 */
	private static $name = 'Queen_MinKabu_';

	/**
	 * ClassBeeQueenMinKabu
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeQueenMinKabu()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$this->__toString();
	}

	/**
	 * __toString
	 *
	 * @access public
	 * @return void
	 */
	public function __toString()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		parent::__toString();
		return $this->url;
	}

	/**
	 * queenName - MinKabu queen
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function queenName()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$name;
	}

	function __destruct(){}
}
