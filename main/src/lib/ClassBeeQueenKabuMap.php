<?php

/**
 * ClassBeeQueenKabuMap
 *
 * @uses ClassBeeQueen
 * @package
 * @version 1.1
 * @date Sat Jul  4 20:25:11 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeQueenKabuMap extends ClassBeeQueen
{
	/**
	 * url - KabuMap url
	 *
	 * @var string
	 * @access private
	 */
	private $url = 'http://jp.kabumap.com/servlets/kabumap/Action?SRC=basic/factor/base&codetext=';

	/**
	 * name - the name of the queen
	 *
	 * @var string
	 * @access private
	 */
	private static $name = 'Queen_KabuMap_';

	/**
	 * ClassBeeQueenKabuMap
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeQueenKabuMap()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$this->__toString();
	}

	/**
	 * __toString
	 *
	 * @access public
	 * @return void
	 */
	public function __toString()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		parent::__toString();
		return $this->url;
	}

	/**
	 * queenName - KabuMap queen
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function queenName()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$name;
	}

	function __destruct(){}
}
