<?php

/**
 * ClassBeeQueenTSE
 *
 * @uses ClassBeeQueen
 * @package
 * @version 1.1
 * @date Sat Jul  4 20:27:24 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */
class ClassBeeQueenTSE extends ClassBeeQueen
{
	/**
	 * url - this is the page of the latest news
	 *
	 * @var string
	 * @access private
	 */
	private $url = 'https://www.release.tdnet.info/inbs/I_list_001_';
	private static $base_url = 'https://www.release.tdnet.info/inbs/';

	/**
	 * name - the name of the queen
	 *
	 * @var string
	 * @access private
	 */
	private static $name = "Queen_TSE_";

	/**
	 * ClassBeeQueenTSE
	 *
	 * @access public
	 * @return void
	 */
	function ClassBeeQueenTSE()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$this->__toString();
	}

	/**
	 * __toString
	 *
	 * @access public
	 * @return void
	 */
	public function __toString()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		parent::__toString();
		return $this->url.date('Ymd');
	}

	/**
	 * queenName - returns the queen name
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function queenName()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$name;
	}

	public static function getBaseUrl()
	{
		Logger::getLogger('file')->trace("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$base_url;
	}

	function __destruct(){}
}
