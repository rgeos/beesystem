<?php

/**
 * ClassBeeQueenjpubb
 *
 * @uses ClassBeeQueen
 * @package
 * @version 0.3
 * @date Sat Jul  4 20:23:24 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

class ClassBeeQueenJpubb extends ClassBeeQueen
{

	/**
	 * $page - page identifier. Default '1'
	 *
	 * @var string
	 * @access private
	 */
	private static $page = '1';

	/**
	 * URL of the companies
	 * @var url
	 */
	private $url = 'http://www.jpubb.com/list/list.php?listed=1';

	private static $name = 'Queen_Jpubb_';

	function ClassBeeQueenJpubb()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$this->__toString();
	}

	/**
	 * __toString
	 *
	 * @access public
	 * @return void
	 */
	public function __toString()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		parent::__toString();
		return $this->url;
	}

	/**
	 * queenName - Jpubb queen
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function queenName()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$name;
	}

	public static function getListed()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$arg = func_get_arg(0);
		if(!isset($arg))
		{
			Logger::getLogger('file')->fatal("\tFunction ".__FUNCTION__." takes 1 string argument - ".BN);
			exit("Function ".__FUNCTION__." takes 1 string argument.");
		}
		else
		{
			$arg = strtolower($arg);
			switch ($arg)
			{
		case 'tokyo':
		case 'nagoya':
		case 'sapporo':
		case 'fukuoka':
			return self::$url_listed.self::$exchanges[$arg];
			break;

		default:
			Logger::getLogger('file')->fatal("\t{$arg} - No such exchange. Valid options are: 'tokyo','nagoya','sapporo','fukuoka' - ".BN);
			exit("{$arg} - No such exchange. Valid options are: 'tokyo','nagoya','sapporo','fukuoka'");
			break;
			}
		}

	}

	public static function setPage($newPage)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$newPage = "&pageID=".$newPage;
		self::$page = $newPage;
	}

	/**
	 * getPage
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function getPage()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		return self::$page;
	}

	function __destruct(){}

}