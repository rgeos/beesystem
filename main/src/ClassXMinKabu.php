<?php

/**
 * ClassXMinKabu
 *
 * @package
 * @version 1.8
 * @date Sat Aug  8 12:55:24 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

include_once SERVERSETTINGSFILE;

class ClassXMinKabu
{

	private static $result;

	/**
	 * ClassXMinKabu
	 *
	 * @access public
	 * @return void
	 */
	function ClassXMinKabu()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
	}

	// TODO - implement this method in the ClassBeeDrone and extend from there
	/**
	 * drone_MinKabu - gets the whole page of the designated stock
	 *
	 * @access private
	 * @return void
	 */
	public static function drone_MinKabu()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$args = func_get_args();

		new ClassBeeWorker();

		$page = new ClassBeeQueenMinKabu();
		if($page == NULL)
		{
			Logger::getLogger('cli')->fatal("\tPage is not set - ".BN);
			exit("Page is not set!");
		}
		else
			Logger::getLogger('file')->debug("\tPage: {$page} - ".BN);

		$stock = ClassBeeQueenMinKabu::setStock($args[0]);
		if($stock == NULL)
		{
			Logger::getLogger('cli')->fatal("\tStock is not set - ".BN);
			exit("Stock is not set!");
		}

		Logger::getLogger('file')->info("\tGetting the stock: {$stock} - ".BN);
		self::$result = ClassBeeWorker::getPage($page.$stock);

		return new self();
	}

	/**
	 * curator_MinKabu - curates content from MinKabu Queen
	 *
	 * @access private
	 * @return void
	 */
	public static function curator_MinKabu()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		// the whole page from the drone
		$html = self::$result;
		$stock = ClassBeeQueenMinKabu::getStock();

		// checking if id=sw_research exists
		$research = array();
		foreach ($html->find('[id=sw_research]') as $element)
			$research['test'] = str_replace(' ', '', $element->plaintext);

		/**
		 * evaluation of the stock
		 *
		 * [evaluation] => Array
         * (
         *    [position] => undervalued
         *    [trend] => sell_0_3
		 *  )
		 */
		$evaluation = array();
		$results_evaluation = $html->find('h3.sr_plt a');

		/**
		 * current and predicted price of the stock
		 *
		 * [prices] => Array
		 * 	(
		 *  	[close] => 471
		 *  	[analysis] => 1,108
		 *  	[market_feeling] => 471
		 * 	)
		 */
		$prices = array();
		$results_price = $html->find('ul.c_ul li.tar b');

		//$null = ClassBeeQueen::printNullArray($research);
		if(!array_filter($research))
		{
			Logger::getLogger('file')->warn("\tNo research for stock {$stock} - ".BN);
			$evaluation['position'] = null; // this does not exist if id=sw_research doesn't exist
			$evaluation['trend'] = ClassTranslations::translate($results_evaluation[0]->find('img',0)->alt); // this is [0] if id=sw_research doesn't exist

			$price['close'] = ClassTranslations::remove_commas($results_price[0]->plaintext);
			$price['prediction'] = null; // this does not exist if id=sw_research doesn't exist
			$price['market_feeling'] = ClassTranslations::remove_commas($results_price[1]->plaintext); // this is [1] if id=sw_research doesn't exist
		}
		else
		{
			$evaluation['position'] = ClassTranslations::translate($results_evaluation[0]->find('img',0)->alt); // this does not exist if id=sw_research doesn't exist
			$evaluation['trend'] = ClassTranslations::translate($results_evaluation[1]->find('img',0)->alt); // this is [0] if id=sw_research doesn't exist

			$price['close'] = ClassTranslations::remove_commas($results_price[0]->plaintext);
			$price['analysis'] = ClassTranslations::remove_commas($results_price[1]->plaintext); // this does not exist if id=sw_research doesn't exist
			$price['market_feeling'] = ClassTranslations::remove_commas($results_price[3]->plaintext); // this is [1] if id=sw_research doesn't exist
		}

		/**
		 * how many people would buy/sell
		 * [votes] => Array
         * 	(
         *   	[buy] => 31
         *   	[sell] => 2
		 * 	)
		*/
		$votes = array();

		$results_buy = $html->find('span[class=fcor fwb fsxl]');
		$votes['buy'] = ClassTranslations::remove_commas($results_buy[0]->plaintext);

		$results_sell = $html->find('span[class=fcgr fwb fsxl]');
		$votes['sell'] = ClassTranslations::remove_commas($results_sell[0]->plaintext);

		/**
		 * stock indeces
		 * 	[index] => Array
		 * 		(
         *   		[volume] => 2800
         *   		[open] => 445.0
         *   		[high] => 445.0
         *   		[low] => 440.0
         *   		[market_value] => 1843000000
         *   		[units] => 100
         *   		[issued_shares] => 4171000
         *   		[dividend_yield] =>
         *   		[payout_ratio] =>
         *   		[PER] => 24.44
         *   		[post_adjustment_PER] => 24.44
         *   		[PSR] => 0.83
         *   		[PBR] => 1.63
         *		)
		 */
		$index = array();
		foreach($html->find('div[class=sh_basic] div[class=clearfix] table[class*=r] tr') as $element)
			if(!empty($element->find('th',0)->plaintext))
				$index[ClassTranslations::translate($element->find('th',0)->plaintext)] = ClassTranslations::remove_commas($element->find('td',0)->plaintext);

		/**
		 *  Report of the above results
		 */
		$report =
		[
		"evaluation" => $evaluation,
		"price" => $price,
		"votes" => $votes,
		"index" => $index
		];

		/**
		 * check if there is any null value in the array
		 */
		$null = ClassBeeQueen::printNullArray($report);
		if(!empty($null))
			Logger::getLogger('file')->warn("\tThe following keys have null values for stock {$stock}: {$null} - ".BN);


		return $report;

	}

	/**
	 * run - method called by the GearmanManager
	 *
	 * @param GearmanJob $job
	 */
	public static function run(GearmanJob $job)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$data = self::drone_MinKabu($job->workload())->curator_MinKabu();

		// TODO - write a nicer mapping function
		$a					= ClassMongoMapper::arraySliceByIndex("evaluation", $data);
		$b["evaluation"]	= ClassMongoMapper::arraySliceByIndex("price", $data);
		$c["evaluation"]	= ClassMongoMapper::arraySliceByIndex("votes", $data);
		$d["index"]			= ClassMongoMapper::arraySliceByIndex("close", $data["price"]);
		$e					= ClassMongoMapper::arraySliceByIndex("index", $data);

		$date = new DateTime("now");

		$payload["source"] = get_class();
		$payload["ticker"] = ['ticker' => intval($job->workload())];
		$payload["updated"] = ['updated' => $date->format('c')];
		$payload["data"] = array_merge_recursive($a, $b, $c, $d, $e);

		ClassMongoWrapper::setCollection('jp_stocks');
		ClassMongoWrapper::updateDb($payload['ticker'], ['$set' => $payload['data']]);
		ClassMongoWrapper::updateDb($payload['ticker'], ['$set' => $payload['updated']]);

		// the following block is the daily data which will be stored as historical data
		$payload["daily"] = [
			"date"		=> $date->format('c'),
			"values"	=> [
				"open"	=> $payload["data"]["index"]["open"],
				"high"	=> $payload["data"]["index"]["high"],
				"low"	=> $payload["data"]["index"]["low"],
				"close"	=> $payload["data"]["index"]["close"],
				"volume"=> $payload["data"]["index"]["volume"]
			]
		];
		ClassMongoWrapper::setCollection('code_'.$payload['ticker']['ticker']);
		ClassMongoWrapper::insertDb($payload["daily"], 'regular');
	}

	function __destruct(){}
}
