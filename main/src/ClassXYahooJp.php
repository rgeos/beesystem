<?php

/**
 * ClassXYahooJp
 *
 * @package
 * @version 1.8
 * @date Tue Aug 11 12:30:57 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

include_once SERVERSETTINGSFILE;

class ClassXYahooJp
{
	private static $result;
	public static $page = 1;

	/**
	 * ClassXYahooJp
	 *
	 * @access public
	 * @return void
	 */
	function ClassXYahooJp()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
	}

	/**
	 * drone_YahooJp - always returns the contents of the 1st page of a desigated stock
	 *
	 * @access private
	 * @return void
	 */
	public static function drone_YahooJp()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$arg = func_get_args();

		$base_url = new ClassBeeQueenYahooJp();

		ClassBeeQueenYahooJp::setStock($arg[0]);
		Logger::getLogger('file')->debug("\tWorking with STOCK ".ClassBeeQueenYahooJp::getStock()." - ".BN);

		if($arg[0] == NULL)
		{
			Logger::getLogger('cli')->fatal("\tTicker is not set for STOCK ".BN);
			exit("\tStock is not set!");
		}

		if ($base_url == NULL) {
			Logger::getLogger('cli')->fatal("\tURL is not set for STOCK ".ClassBeeQueenYahooJp::getStock()." - ". BN);
			exit("\tURL is not set!");
		}

		if	(
			(isset($arg[1]) or isset($arg[2])) and
			(preg_match("/\d{8}/",$arg[1]) or preg_match("/\d{8}/", $arg[2]))
			)
		{
		// TODO - both dates should be \d{8} also start < end
			Logger::getLogger('file')->debug("\tStart date is set to: {$arg[1]} / End date is set to: {$arg[2]} - ".BN);
			ClassBeeQueenYahooJp::setDate($arg[1],'start');
			ClassBeeQueenYahooJp::setDate($arg[2],'end');
		}
		else
		{
			Logger::getLogger('file')->debug("\tAutomatic set date - ".BN);
			ClassBeeQueenYahooJp::setDate((new DateTime())->sub(new DateInterval('P1D'))->format('Ymd'),'start');
			ClassBeeQueenYahooJp::setDate((new DateTime())->format('Ymd'),'end');
		}

		if(isset($arg[3]))
		{
			Logger::getLogger('file')->debug("\tInterval type is {$arg[3]} - ".BN);
			ClassBeeQueenYahooJp::setIntervalType($arg[3]);
		}
		else
		{
			Logger::getLogger('file')->debug("\tInterval type is automatically set to day - ".BN);
			ClassBeeQueenYahooJp::setIntervalType('day');
		}

		if(isset($arg[4]))
		{
			Logger::getLogger('file')->debug("\tData type is {$arg[4]} - ".BN);
			ClassBeeQueenYahooJp::setDataType($arg[4]);
		}
		else
		{
			Logger::getLogger('file')->debug("\tData type is automatically set to series - ".BN);
			ClassBeeQueenYahooJp::setDataType('');
		}

		$stock = ClassBeeQueenYahooJp::getStock();
		$start = ClassBeeQueenYahooJp::getDate('start');
		$end = ClassBeeQueenYahooJp::getDate('end');
		$interval_type = ClassBeeQueenYahooJp::getIntervalType();
		$data_type = ClassBeeQueenYahooJp::getDataType();

		$url = $base_url.$data_type.$stock.$start.$end.$interval_type.ClassBeeQueenYahooJp::getPage();

		Logger::getLogger('file')->info("\tGetting the stock: {$stock} - ".BN);
		Logger::getLogger('file')->debug("\tStart: {$start} / End: {$end} / Interval type: {$interval_type} / Data type: {$data_type} - ".BN);
		Logger::getLogger('file')->debug("\tURL: {$url} - ".BN);

		//return $base_url;
		self::$result = ClassBeeWorker::getPage($url);
		return new self();
	}

	/**
	 * getNumberPages - calculates how many pages does the client have
	 *
	 * @param mixed $page
	 * @access public
	 * @return void
	 */
	public static function getNumberPagesYahoo()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$page = self::$result;

		if($page == NULL)
		{
			Logger::getLogger('cli')->fatal("\tPage is not set for STOCK: ".ClassBeeQueenYahooJp::getStock()." - ".BN);
			exit("\tPage is not set!");
		}

		$analysis = array();
		foreach($page->find('span[class="stocksHistoryPageing yjS"]') as $element)
			$analysis = preg_split("#[\s/～件中]+#",$element->plaintext, -1, PREG_SPLIT_NO_EMPTY);

		if ($analysis[1] == NULL){
			Logger::getLogger('file')->warn("\tThere may be no data for the range - STOCK: ".ClassBeeQueenYahooJp::getStock()." - ".BN);
			return -1;
		}
		else
			Logger::getLogger('file')->debug("\tParam 1: {$analysis[1]} / Param 2: {$analysis[2]} - ".BN);

		return ceil($analysis[2]/$analysis[1]);
	}

	/**
	 * curator_YahooJp - cleanup of the content
	 *
	 * @access private
	 * @return void
	 */
	public static function curator_YahooJp()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$html = self::$result;
		$stock = ClassBeeQueenYahooJp::getStock();

		if($html == NULL)
		{
			Logger::getLogger('cli')->fatal("\tPage is not set for {$stock} - ".BN);
			break;
			//exit("\tPage is not set!");
		}

		$table = array();
		$data_type = ClassBeeQueenYahooJp::getDataType();
		if($data_type === 'margin/')
		{
			Logger::getLogger('file')->debug("\tI am of type {$data_type} - ".BN);
			foreach($html->find('table[class="boardFin yjSt marB6"] tr') as $element)
			{
				if($element->find('td', 0) != null)
				{
					$item = ClassTranslations::clean_date($element->find('td',0)->plaintext);
					$item['values']['short'] = ClassTranslations::remove_commas($element->find('td',1)->plaintext);
					$item['values']['long'] = ClassTranslations::remove_commas($element->find('td',2)->plaintext);
					$item['values']['short_delta'] = ClassTranslations::remove_commas($element->find('td',3)->plaintext);
					$item['values']['long_delta'] = ClassTranslations::remove_commas($element->find('td',4)->plaintext);
					$item['values']['ratio'] = ClassTranslations::remove_commas($element->find('td',5)->plaintext);
					$table[] = $item;
				}
			}
		}
		else
		{
			Logger::getLogger('file')->debug("\tI am of type {$data_type} - ".BN);
			foreach($html->find('table[class="boardFin yjSt marB6"] tr') as $element)
			{
				if($element->find('td', 0) != null)
				{
					$item = ClassTranslations::clean_date($element->find('td',0)->plaintext);
					$item['values']['open'] = ClassTranslations::remove_commas($element->find('td',1)->plaintext);
					$item['values']['high'] = ClassTranslations::remove_commas($element->find('td',2)->plaintext);
					$item['values']['low'] = ClassTranslations::remove_commas($element->find('td',3)->plaintext);
					$item['values']['close'] = ClassTranslations::remove_commas($element->find('td',4)->plaintext);
					$item['values']['volume'] = ClassTranslations::remove_commas($element->find('td',5)->plaintext);
					$table[] = $item;
				}
			}
		}


		/**
		 * check if there is any null value in the array
		 */
		$null = ClassBeeQueen::printNullArray($table);
		if(!empty($null))
			Logger::getLogger('file')->warn("\tThe following keys have null values for stock {$stock}: {$null} - at page: ".ClassBeeQueenYahooJp::getPage()." - ".BN);
		return $table;
	}

	/**
	 * run - method called by the GearmanManager
	 *
	 * @param GearmanJob $job
	 */
	public static function run(GearmanJob $job)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$param = json_decode($job->workload(),true);
		$pages = self::drone_YahooJp($param['ticker'], $param['start'], $param['end'], $param['interval'], $param['type'])->getNumberPagesYahoo();

		if($pages<0)
		{
			Logger::getLogger('file')->fatal("\tRange fail - ".$param['start']." ~ ".$param['end']." Task removed for STOCK ".$param['ticker']);
			$job->sendFail();
		}

		$date = new DateTime("now 00:00:00");
		ClassMongoWrapper::setCollection('code_'.$param['ticker']);

		$payload["data"] = [];
		for($i = 1; $i <= $pages; $i++)
		{
			self::$page = ClassBeeQueenYahooJp::setPage($i);
			$payload["source"]	= get_class();
			$payload["ticker"]	= ['ticker' => intval($param['ticker'])];
			$payload["updated"]	= ['updated' => $date->format('c')];
			$payload["data"]	= array_merge($payload["data"], self::drone_YahooJp($param['ticker'], $param['start'], $param['end'], $param['interval'], $param['type'])->curator_YahooJp());
		}

		ClassMongoWrapper::insertDb($payload["data"], 'batch');

		self::$page = ClassBeeQueenYahooJp::setPage(1);
	}

	function __destruct(){}
}
