<?php

/**
 * Client_TSE
 *
 * @package
 * @version 1.1
 * @date Sat Jul  4 20:38:41 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

// TODO - this class needs to be fixed and controlled from ../automate.php

class Client_TSE
{
	/**
	 * Client_TSE
	 *
	 * @access public
	 * @return void
	 */
	function Client_TSE()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		if(self::drone_TSE())
			//self::atom_TSE();
			self::rss_TSE();
		else
			echo "No work for the Worker Bee".PHP_EOL;
	}

	/**
	 * drone_TSE	- it will fetch/compare/merge the headers from the website privided by the Queen
	 *				- it will create a file and store the data
	 *
	 * @access public
	 * @return void
	 */
	private function drone_TSE()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		new ClassBeeDrone();
		$url_TSE = new ClassBeeQueenTSE();
		ClassBeeDrone::setUrl($url_TSE);

		for ($i = 0; $i < 10; $i++)
		{
			// $date_TSE[] = ClassBeeDrone::hashString(ClassBeeDrone::getHeaders($url_TSE, "Last-Modified"));
			// $etag_TSE[] = ClassBeeDrone::hashString(ClassBeeDrone::getHeaders($url_TSE, "ETag"));

			// it's faster without hashing -TODO should I leave it like this????
			$date_TSE[] = ClassBeeDrone::getHeaders($url_TSE, "Last-Modified");
			$etag_TSE[] = ClassBeeDrone::getHeaders($url_TSE, "ETag");
		}

		$fetchedArray = array();

		$filename = ClassBeeQueenTSE::queenName().date('Ymd').".txt";

		if(!file_exists($filename))
		{
			// create the file with empty array
			ClassBeeQueenTSE::storeData($fetchedArray, $filename);

			// write to the file whatever
			// echo "We have no file, so we created this".PHP_EOL;
			$fetchedArray = ClassBeeQueenTSE::getData($date_TSE, $etag_TSE);
			ClassBeeQueenTSE::storeData($fetchedArray, ClassBeeQueenTSE::queenName().date('Ymd').".txt");

			// ask the queen to order the worker
			return true;
		}
		else
		{
			// fetch from TSE & retrieve from file
			// echo "Fetched from TSE".PHP_EOL;
			$fetchedArray = ClassBeeQueenTSE::getData($date_TSE, $etag_TSE);
			// echo "Retrieved from file".PHP_EOL;
			$retrievedArray = ClassBeeQueenTSE::retreiveData($filename);


			if(!strcasecmp(serialize(array_keys($fetchedArray)), serialize(array_keys($retrievedArray))) == 0)
			{
				//directly create the new file in case timestamps are different
				ClassBeeQueenTSE::storeData($fetchedArray, ClassBeeQueenTSE::queenName().date('Ymd').".txt");
				return true;
			}
			else
			{
				// compare
				// echo "Comparison array".PHP_EOL;
				$comparedArray = ClassBeeQueenTSE::compareArray($fetchedArray,$retrievedArray, "diff");

				if($comparedArray[ClassBeeQueenTSE::retrieveKey($comparedArray)] != null)
				{
					// merge
					// echo "Merged array".PHP_EOL;
					$mergedArray = ClassBeeQueenTSE::compareArray($comparedArray, $retrievedArray, "merge");

					// store merged array
					ClassBeeQueenTSE::storeData($mergedArray, ClassBeeQueenTSE::queenName().date('Ymd').".txt");
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}

	/**
	 * worker_TSE - returns the whole page content
	 *
	 * @access public
	 * @return void
	 */
	private function worker_TSE()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		new ClassBeeWorker();
		// following 2 are for test purpose
		//return ClassBeeWorker::getPage("https://www.release.tdnet.info/inbs/I_list_001_20131214.html");
		//return ClassBeeWorker::getPage("https://www.release.tdnet.info/inbs/I_list_001_20131211.html");
		return ClassBeeWorker::getPage(new ClassBeeQueenTSE());
	}

	/**
	 * curator_TSE	- it will clean the contents of the page provided by the worker_TSE
	 *
	 * @access private
	 * @return void
	 */
	private function curator_TSE()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$html = self::worker_TSE();

		// curate the content - TODO prettify
		$reports = array();
		foreach ($html->find('tr') as $key) {

			$item1 = $key->find('td[class*=new]',0);
			$item2 = $key->find('td a.style006',0);
			$item3 = $key->find('td a.style002',0);

			// if there is no 'time' value we are not interested in the entry
			if($item1 != null)
			{
				// time of the posting
				$item['time'] = trim($key->find('td[class*=new]',0)->plaintext);

				// code of the company
				$item['code'] = trim($key->find('td[class*=new]',1)->plaintext);

				// name of the company
				$item['name'] = trim($key->find('td[class*=new]',2)->plaintext);

				// information about the posting
				$item['detail'] = trim($key->find('td[class*=new]',3)->plaintext);

				// link to the posting
				$item['link'] = ClassBeeQueenTSE::getBaseUrl().trim($key->find('td a[href*=pdf]',0)->href);

				// link to the forcast document
				$item2 != null ? $item['prediction'] = ClassBeeQueenTSE::getBaseUrl().trim($key->find('td a.style006',0)->href) : $item['prediction'] = null;

				// link to the financial report in XBRL
				$item3 != null ? $item['xbrl'] = ClassBeeQueenTSE::getBaseUrl().trim($key->find('td a.style002',0)->href) : $item['xbrl'] = null;

				// what market is the company listed
				$item['market'] = trim($key->find('td[class*=new]',6)->plaintext);

				// TODO - need to check for the update
				// $item['update'] = trim($key->find('td[class*=upd]',7)->plaintext);

				$reports[] = $item;
			}
		}

		return $reports;
		//print_r($reports);

	}

	/**
	 * rss_TSE - creates RSS2 feeds
	 * generates feed in file RSS2.xml
	 * TODO - extend capabilities to Atom feed
	 *
	 * @access private
	 * @return void
	 */
	private function rss_TSE()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$curated = self::curator_TSE();

		$rss = new FeedWriter(RSS2);

		$rss->setTitle('Tokyo Stock Exchange Latest Releases');
		$rss->setLink('http://localhost/');
		$rss->setDescription('Latest public disclosures from TSE');

		//Image title and link must match with the 'title' and 'link' channel elements for RSS 2.0
		//$rss->setImage('Testing the RSS writer class','http://www.ajaxray.com/projects/rss','http://www.rightbrainsolution.com/images/logo.gif');

		//Use core setChannelElement() function for other optional channels
		$rss->setChannelElement('language', 'jp');
		$rss->setChannelElement('pubDate', date(DATE_RSS, time()));

		for ($i = 0; $i < count($curated); $i++)
		{
			$item = $rss->createNewItem();
			$item->setTitle($curated[$i]['name']);
			$item->setLink($curated[$i]['link']);
			$item->setDate($curated[$i]['time']);
			$item->addElement('detail',$curated[$i]['detail']);
			$item->setDescription("
				<p>Company:\t<b>".$curated[$i]['name']."</b>
				<br>Code:\t<b>".$curated[$i]['code']."</b>
				<br>Prediction:\t<b>".$curated[$i]['prediction']."</b>
				<br>XBRL:\t<b>".$curated[$i]['xbrl']."</b>
				<br>Market:\t<b>".$curated[$i]['market']."</b></p>");
			$rss->addItem($item);
		}

		$rss->genarateFeed('RSS2.xml');

	}

	function __destruct() {}
}

//new Client_TSE();
