<?php

/**
 * ClassXKabuMap
 *
 * @package
 * @version 1.7
 * @date Sat Aug  8 15:28:39 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

include_once SERVERSETTINGSFILE;

class ClassXKabuMap
{

	private static $result;

	/**
	 * ClassXKabuMap
	 *
	 * @access public
	 * @return void
	 */
	function ClassXKabuMap()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
	}

	// TODO - implement this method in the ClassBeeDrone and extend from there
	/**
	 * drone_KabuMap - retrieves the whole contents of the page received from the queen
	 *
	 * @access public
	 * @return void
	 */
	public static function drone_KabuMap()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$args = func_get_args();

		new ClassBeeWorker();

		$page = new ClassBeeQueenKabuMap();
		if($page == NULL)
		{
			Logger::getLogger('cli')->fatal("\tPage is not set - ".BN);
			exit("Page is not set!");
		}
		else
			Logger::getLogger('file')->debug("\tPage: {$page} - ".BN);

		$stock = ClassBeeQueenKabuMap::setStock($args[0]);
		if($stock == NULL)
		{
			Logger::getLogger('cli')->fatal("\tStock is not set - ".BN);
			exit("Stock is not set!");
		}

		Logger::getLogger('file')->info("\tGetting the stock: {$stock} - ".BN);
		self::$result = ClassBeeWorker::getPage($page.$stock);

		return new self();
	}

	/**
	 * curator_KabuMap - cleans up the page from the worker
	 *
	 * @access public
	 * @return void
	 */
	public static function curator_KabuMap()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		// the whole page from the drone
		$html = self::$result;
		$stock = ClassBeeQueenKabuMap::getStock();

		/**
		 * quant values of the stock
		 *
		 *  [quant] => Array
		 *  	(
		 *  		[score] => 50
		 *  		[industry_average] => 53
		 *  	)
		 */
		$quant = array();

		$results_score = $html->find('li[id=totalRow] strong');
		$quant['score'] = ClassTranslations::remove_commas($results_score[0]->plaintext);

		$results_industry_average = $html->find('li[id=sectorRow] strong');
		$quant['industry_average'] = ClassTranslations::remove_commas($results_industry_average[0]->plaintext);

		/**
		 * other technical analysis
		 *
		 *	[analysis] => Array
		 *		(
         *   		[growth] => 0.8
         *   		[valuation] => 0.6
         *   		[scale] => 0.1
         *   		[technical] => 0.5
         *   		[solvency] => 0.6
         *   		[trend] => 0.4
         *   	)
		 */
		$analysis = array();
		foreach($html->find('ul li.scoreBase') as $element)
		{
			$item = preg_split("# #",$element->plaintext, -1, PREG_SPLIT_NO_EMPTY);
//			$analysis[] = ['attribute' => ClassTranslations::translate($item[0]), 'value' => ClassTranslations::calculate($item[1])];
			$analysis[ClassTranslations::translate($item[0])] = ClassTranslations::calculate($item[1]);
		}

		/**
		 *  report of the above results
		 */
		$report =
		[
		"analysis" => $analysis,
		"quant" => $quant
		];

		/**
		 * check if there is any null value in the array
		 */
		$null = ClassBeeQueen::printNullArray($report);
		if(!empty($null))
			Logger::getLogger('file')->warn("\tThe following keys have null values for stock {$stock}: {$null} - ".BN);

		return $report;
	}

	/**
	 * run - method called by the GearmanManager
	 *
	 * @param GearmanJob $job
	 */
	public static function run(GearmanJob $job)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$data = self::drone_KabuMap($job->workload())->curator_KabuMap();

		// TODO - write a nicer mapping function
		$a				= ClassMongoMapper::arraySliceByIndex("analysis", $data);
		$b["analysis"]	= ClassMongoMapper::arraySliceByIndex("quant", $data);

		$date = new DateTime("now");

		$payload["source"] = get_class();
		$payload["ticker"] = ['ticker' => intval($job->workload())];
		$payload["updated"] = ['updated' => $date->format('c')];
		$payload["data"] = array_merge_recursive($a,$b);

		ClassMongoWrapper::setCollection('jp_stocks');
		ClassMongoWrapper::updateDb($payload['ticker'], ['$set' => $payload['data']]);
		ClassMongoWrapper::updateDb($payload['ticker'], ['$set' => $payload['updated']]);
	}

	function __destruct(){}
}
