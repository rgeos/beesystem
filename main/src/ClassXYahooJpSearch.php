<?php

/**
 * ClassXSearch
 *
 * @package
 * @version 0.1
 * @date Sat Nov  7 18:17:41 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

include_once SERVERSETTINGSFILE;

class ClassXYahooJpSearch {

	private static $result;
	private static $base_url = 'http://stocks.finance.yahoo.co.jp/stocks/detail/?code=';

	function ClassXYahooJpSearch(){}

	public static function drone_YahooJpSearch()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$stock	= ClassBeeQueenYahooJp::getStock();
		$url		=  self::$base_url.$stock;
		self::$result = ClassBeeWorker::getPage($url);
		return new self();
	}

	public static function curator_YahooJpSearch()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$html = self::$result;

		return TRUE;
	}

	public static function run(GearmanJob $job)
	{
		return TRUE;
	}
}
