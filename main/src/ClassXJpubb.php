<?php

/**
 * ClassXJpubb
 *
 * @package
 * @version 0.6
 * @date Sat Aug  8 15:40:28 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

include_once SERVERSETTINGSFILE;

class ClassXJpubb
{

	private static $result;
	public static $page = 1;

	/**
	 * ClassXJpubb
	 *
	 * @access public
	 * @return void
	 */
	function ClassXJpubb()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
	}

	/**
	 * drone_Jpubb - retrieves the whole contents of the page received from the queen
	 *
	 * @access public
	 * @return void
	 */
	public static function drone_Jpubb()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$base_url = new ClassBeeQueenJpubb();

		$url = $base_url.ClassBeeQueenJpubb::getPage();

		self::$result = ClassBeeWorker::getPage($url);
		return new self();
	}

	/**
	 * getNumberPages - calculates how many pages does the client have
	 *
	 * @param mixed $page
	 * @access public
	 * @return void
	 */
	public static function getNumberPagesJpubb()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$page = self::$result;

		$item = '';
		foreach ($page->find('div[id=contents]') as $elem)
			$item = $elem->find('div', 6)->plaintext;

		preg_match_all('/(\d+)件/', $item, $matches);

		return ceil(intval($matches[1][0])/intval($matches[1][2]));
	}

	/**
	 * curator_Jpubb - cleans up the page from the worker
	 *
	 * @access public
	 * @return void
	 */
	public static function curator_Jpubb()
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);

		$html = self::$result;

		$items = array();
		foreach($html->find('table[class="corpTable"] tr[class*=fill]') as $elem)
		{
			$item['listed']				= true;
			$item['ticker']				= intval(ClassTranslations::remove_commas($elem->find('td',0)->plaintext));

			if($item['ticker'] == null) $item['listed'] = false;

			$infos						= $elem->find('td', 2)->plaintext;
			$info						= preg_split('/\|\s*/', $infos);

			$item['info']['name']['jp']	= $elem->find('td', 1)->plaintext;
			$item['info']['name']['en']	= null;
			$item['info']['industry']	= ClassTranslations::translate(trim($info[0]));
			$item['info']['market']		= ClassTranslations::multiTranslate(',',trim($info[1]));
			$item['info']['address']	= ClassTranslations::translate(trim($info[2]));
			$item['info']['web']		= $elem->find('td', 2)->find('a', -1)->href;
			$item['others']				= null;

			$items[] = $item;
		}

		return $items;
	}

	/**
	 * run - method called by the GearmanManager
	 *
	 * @param GearmanJob $job
	 */
	public static function run(GearmanJob $job)
	{
		Logger::getLogger('file')->debug("\tMy name is ".__FUNCTION__." and I am called from - ".BN);
		$pages = self::drone_Jpubb()->getNumberPagesJpubb();

		ClassMongoWrapper::setCollection('jp_stocks');

		// TODO- a redisign of scraping data is needed for this page
		$payload["data"] = [];
		for ($i = 0; $i < $pages; $i ++)
		{
			self::$page = ClassBeeQueenJpubb::setPage($i);
			$payload["source"] = get_class();
			$payload["data"] = array_merge($payload["data"], self::drone_Jpubb()->curator_Jpubb());
		}

		ClassMongoWrapper::insertDb($payload['data'], 'batch');
	}

	function __destruct(){}
}
