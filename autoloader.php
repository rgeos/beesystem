<?php

/**
 * Class autoloader
 *
 * @package
 * @version 0.4
 * @date Sat Aug  8 09:47:22 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

date_default_timezone_set('Asia/Tokyo');
set_include_path(dirname(__FILE__));

define('DS', DIRECTORY_SEPARATOR);
define('BN', basename(__FILE__));

/**
 * this file contains:
 * - IP address(es) of Gearman Servers
 * - IP address(es) of MongoDB Servers
 * - credentials of MongoDB
 * - default DB and Collection
 */
define('SERVERINIFILE', get_include_path().DS.'server.ini');

/**
 * this file contains:
 * - GearmanManager settings
 */
define('MANAGERINIFILE', get_include_path().DS.'config.ini');

/**
 * this file will set the default access to the mongoDB
 */
define('SERVERSETTINGSFILE', get_include_path().DS.'ServerSettings.php');

/**
 * Autoregistering class functions
 */

spl_autoload_register(function($class)
{
	$dirs =
	[
		dirname(__FILE__).DS.'main/sys/',		// beesys main classes
		dirname(__FILE__).DS.'main/src/',		// data sources
		dirname(__FILE__).DS.'main/src/lib/',	// helper libraries for data sources
		dirname(__FILE__).DS.'main/etc/',		// helper libraries for the whole system
		dirname(__FILE__).DS.'main/etc/manager/',// helper libraries for GearmanManager
		dirname(__FILE__).DS.'main/etc/mongo/',	// helper libraries for MongoDB
		dirname(__FILE__).DS.'main/etc/rss/',	// helper libraries for RSS
		dirname(__FILE__).DS.'main/log4php/'		// debugging
	];

	foreach($dirs as $dir)
	{
		$file = $dir.$class.'.php';
		if(file_exists($file))
		{
			require_once $file;
			return;
		}
	}
});