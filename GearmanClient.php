#!/usr/bin/env php

<?php

/**
* Part of the Gearman message queueing framework - client
*
* @package
* @version 1.1
* @date Sun Nov  8 12:09:45 JST 2015
* @copyright 2015 erageor
* @author George S. Radescu <george.serban.radescu@gmail.com
* @license GPL3
*/

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'autoloader.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'ServerSettings.php';
Logger::configure(['file' => 1, 'cli' => 2], new ClassLogger());

define('E', PHP_EOL);
const VERSION = "2.0.0";

$opts_short	= "d::h::p::v::";
$opts_long	= ["client:", "min:", "max:", "server:"];
$opts_short_trim =array_filter(explode("::", $opts_short));
$opts_long_trim = array_map('trim', $opts_long, str_split(str_repeat(":", count($opts_long))));

$opts = getopt($opts_short, $opts_long);



/**
 *	Setting up the gearman client
 *	Connect it with the servers
 *	Set the callback function
 */
$client = new GearmanClient();
//$client->addServer();
$client->addServers(ClassSettingsParser::shuffleArray(ClassSettingsParser::serverSettings('GearmanServers'))); // Due to a bug in the framework, server list hashing doesn't work
$client->addOptions(GEARMAN_CLIENT_FREE_TASKS);

$client->setCreatedCallback("beesys_created");
$client->setWarningCallback("beesys_warning");
$client->setCompleteCallback("beesys_complete");
$client->setFailCallback("beesys_fail");


/**
 * MongoDB search queries and options
 */
//$query = ['listed' => true];																		// these are all listed companies
//$query = ['$and' =>[['listed' => true], ['ticker' => ['$in' => [6591,6166,3191,7203,4593]]]]]; 	// for testing purposes limit the number of companies
//$query = ['$and' =>[['listed' => true], ['ticker' => ['$in' => [3191,7203]]]]];					// for testing purposes limit the number of companies
//$query = ['$and' =>[['listed' => true], ['ticker' => [['$gte' => 0],['$lt' => 2000]]]]];			// for testing purposes limit the number of companies

$query = ['$and' =>[['listed' => true], ['ticker' => ['$gte' => minimum(), '$lte' => maximum()]]]];
$options = ['_id' => 0, 'ticker' => 1];																// show only the field called 'ticker'
$results = ClassMongoWrapper::searchDb($query, $options);											// search the DB





if(null != array_diff($opts_long_trim,array_keys($opts)))
	help("\tYou are missing some required arguments.");

foreach (array_keys($opts) as $opt) switch($opt)
{
	case 'd':			daemon(); break;
	case 'h':			help(); break;
	case 'p':			printing(); break;
	case 'v':			version(); break;
	case 'client':		client(); break;
	case 'min':			minimum(); break;
	case 'max':			maximum(); break;
	case 'server':		server(); break;
	default:
	{
		Logger::getLogger('cli')->error("First CLI option is not set correct!!! We received: \"".$argument."\"");
		help("\tWTF just happen?");
	}
}



/**
 * TODO - this part needs to be either in the CLI or a function call
 * YahooJp options in regards to:
 * - start date
 * - end date
 * - interval
 * - data type
 */
$start		= date('Ymd', strtotime("1983-01-01"));	// from what date to start collecting the data
//$end		= date('Ymd', strtotime("2014-07-31"));	// at what date to stop collecting the data - exact date
$end		= date('Ymd', strtotime("-1 days"));	// at what date to stop collecting the data - relative date
$interval	= 'day';								// interval type options (day, week, month)
$type		= 'regular';							// regular or margin // margin - is not working with intervals less than 3 weeks




/**
 * CLI arguments for each of the sources
 * - minkabu:
 * - kabumap:
 * - yahoo jp:
 * - jpubb:
 * - TSE:
 */
if(null !== client())
{
	switch (client())
	{
		case "jpubb":
			$client->addTaskBackground('ClassXJpubb', "helloJP", null, uniqid("ClassXJPubb - ",true));
			break;
		case "kabumap":
			foreach ($results as $result) $client->addTaskBackground('ClassXKabuMap', "{$result['ticker']}", null, uniqid("ClassXKabuMap - {$result['ticker']} - ", true));
			break;
		case "minkabu":
			foreach ($results as $result) $client->addTaskBackground('ClassXMinKabu', "{$result['ticker']}", null, uniqid("ClassXMinKabu - {$result['ticker']} - ", true));
			break;
		case "yahoojp":
			foreach ($results as $result) $client->addTaskBackground('ClassXYahooJp', json_encode(["ticker" => "{$result['ticker']}", "start" => $start, "end" => $end, "interval" => $interval, "type" => $type]), null, uniqid("ClassXYahooJp - {$result['ticker']} - ",true));
			break;
		case "search":
			foreach ($results as $result) $client->addTaskBackground('ClassXYahooJpSearch', "{$result['ticker']}", null, uniqid("ClassXYahooJpSearch - {$result['ticker']} - ", true));
			break;
		default:
			Logger::getLogger('cli')->fatal("WTF just happen!");
			exit();
	}

	//$client->addTask('MinKabu', "1111"); // TODO - how to handle nonexistent pages WARNING!!! a nonexistent page kills the workers

	$client->runTasks();
}
else
{
	Logger::getLogger('cli')->fatal("\tNo argument was passed to '".basename(__FILE__)."'");
	help("No client name passed as argument");
}


function getGlobal($key)
{
	if(!empty($key) && in_array($key, array_merge($GLOBALS['opts_short_trim'], $GLOBALS['opts_long_trim'])))
	{
		$array = $GLOBALS['opts'];
		return $array[$key];
	}
	else exit("WTF just happen");
}

function daemon()
{
	// TODO - make it run in the background
	echo getmypid().E;
}

function help($msg="")
{
	if($msg)
	{
		echo "ERROR:".E;
		echo "  ".wordwrap($msg, 90, E).str_repeat(E, 2);
	}
	echo "USAGE:".E;
	echo $base1 = "\t# ".basename(__FILE__)." --client NAME | --min NUMBER | --max NUMBER | --server ADDRESS [-d] [-h] [-p] [-v]".E;
	echo "\t".str_repeat("-", strlen($base1)).E;
	echo E;

	echo $base2 = "\tRequired arguments.".E;
	echo "\t".str_repeat("-", strlen($base2)).E;
	echo "\t --client NAME\t\tSpecify the name of the client you wish to run. Possible options: 'yahoojp', 'minkabu', 'kabumap', 'jpubb'.".E;
	echo "\t --min NUMBER\t\tThe lowest value of a company ticker. The relation between min and max is '0 < min <= max'.".E;
	echo "\t --max NUMBER\t\tThe greatest value of a company ticker. The relation between min and max is 'min <= max <= 9999'.".E;
	echo "\t --server ADDRESS\tServer IP address.".E;
	echo E;

	echo $base3 = "\tOptional arguments.".E;
	echo "\t".str_repeat("-", strlen($base3)).E;
	echo "\t -d\t\t\tRun in background.".E;
	echo "\t -h\t\t\tThis help menu.".E;
	echo "\t -p\t\t\tPrint the tickers and exit.".E;
	echo "\t -v\t\t\tSoftware version.".E;
	exit();
}

function printing()
{
	$msg = "\tPrinting the range list of companies and exit".E;
	echo E."\t".str_repeat("=", strlen($msg)).E;
	echo $msg;
	echo "\t".str_repeat("=", strlen($msg)).E;
	print_r($GLOBALS['results']);
	exit();
}

function version()
{
	$version = "\t = BeeSys v.".VERSION." =".E;
	$copy = "\t = Copyright ".date('Y')." =".E;
	echo "\t".str_repeat("=", strlen($version)-1).E;
	echo $version;
	echo $copy;
	echo "\t".str_repeat("=", strlen($version)-1).E;

}

function client()
{
	$client = ['yahoojp', 'minkabu', 'kabumap', 'jpubb', 'tse'];
	if(!empty(getGlobal('client')) && in_array(getGlobal('client'), $client)) // TODO - empty does not work on getGlobal
		return getGlobal('client');
	else
		help("The client ".getGlobal('client')." does not exist.");
}

function minimum()
{
	$min = intval(getGlobal('min'));
	$max = intval(getGlobal('max'));
	if(!(empty($min) || empty($max)) && $min > 0 && $min <= $max)
		return $min;
	else
		help("The combination values of min/max is not correct.");
}

function maximum()
{
	$min = intval(getGlobal('min'));
	$max = intval(getGlobal('max'));
	if(!(empty($min) || empty($max)) && $max >= $min && $max <= 9999)
		return $max;
	else
		help("The combination values of min/max is not correct.");
}

function server()
{
	$ips = array_values(ClassSettingsParser::serverSettings('GearmanServers'));
	if(!empty(getGlobal('server')) && in_array(getGlobal('server'), $ips))
		return getGlobal('server');
	else
		help("The server ".getGlobal('server')." does not exist.");

}






/**
 * Callback function for the creation of task
 * @param GearmanTask $task
 */
function beesys_created($task)
{
	$message = "CREATED:\t".$task->unique()." : ".$task->jobHandle()." - ".date('YmdHis').PHP_EOL;
	Logger::getLogger('file')->info($message);
}

/**
 * Callback function to handle the warning messages from Gearman framework
 * @param GearmanTask $task
 */
function beesys_warning($task)
{
	$message = "STATUS:\t".$task->unique()." : ".$task->jobHandle()." - ".date('YmdHis').PHP_EOL;
	Logger::getLogger('cli')->warn($message);
}

/**
 * Callback function to inform about the successful completion of the task
 * @param GearmanTask $task
 */
function beesys_complete($task)
{
	$message = "COMPLETE:\t".$task->unique()." : ".$task->jobHandle()." - ".date('YmdHis').PHP_EOL;
	Logger::getLogger('file')->info($message);
}

/**
 * Callback function to inform about the failure of the task
 * @param GearmanTask $task
 */
function beesys_fail($task)
{
	$message = "FAILED:\t".$task->unique()." : ".$task->returnCode()." - ".date('YmdHis').PHP_EOL;
	Logger::getLogger('cli')->fatal($message);
}
