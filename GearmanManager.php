#!/usr/bin/env php

<?php

/**
 * Implements the worker portions of the pecl/gearman library
 *
 * @version 0.2
 * @filesource @package GearmanManager by Brian Moon
 * @date Sat Aug  1 15:10:09 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 *
 */

declare(ticks = 1);
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'autoloader.php';

Logger::configure(['file' => 1, 'cli' => 2], new ClassLogger());

$mgr = new GearmanPeclManager();
$mgr->run();