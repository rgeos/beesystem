<?php
/**
 * ServerSettings
 *
 * @package
 * @version 0.2
 * @date Tue Aug 11 13:42:33 JST 2015
 * @copyright 2015 erageor
 * @author George S. Radescu <george.serban.radescu@gmail.com
 * @license GPL3
 */

Logger::configure(['file' => 1, 'cli' => 2], new ClassLogger());

/**
 * Set the server configuration file
 */
ClassSettingsParser::setIniFile(SERVERINIFILE); // TODO - from the cmd line

/**
 * Setting up MongoDB parameters
 * - hostname
 * - username
 * - password
 * - database name
 * - authentication source (normally the same database name)
 * - default collection
 */

ClassMongoWrapper::setHost(ClassSettingsParser::serverSettings('MongoServers', 'mongoHost'));
ClassMongoWrapper::setUsername(ClassSettingsParser::serverSettings('MongoSettings','mongoUser'));
ClassMongoWrapper::setPassword(ClassSettingsParser::serverSettings('MongoSettings', 'mongoPass'));
ClassMongoWrapper::setDbName(ClassSettingsParser::serverSettings('MongoSettings','mongoDb'));
ClassMongoWrapper::setAuthSource(ClassSettingsParser::serverSettings('MongoSettings', 'mongoAuth'));
ClassMongoWrapper::setCollection(ClassSettingsParser::serverSettings('MongoSettings', 'mongoColl'));